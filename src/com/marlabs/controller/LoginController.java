package com.marlabs.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.marlabs.model.User;
import com.marlabs.model.UserValidator;

@Controller
public class LoginController {

	@InitBinder
	public void initBinder(DataBinder binder) {
		binder.setValidator(new UserValidator());
	}

	@RequestMapping("/loginUI")
	public String testLogin() {
		return "login";
	}

	@RequestMapping("/login")
	public String testSubmit(@Valid @ModelAttribute("userForm") User user, BindingResult result) {
		if (result.hasErrors()) {
			return "login";
		}
		System.out.println("username\t" + user.getUsername());
		System.out.println("password\t" + user.getPassword());
		return "success";
	}

}
