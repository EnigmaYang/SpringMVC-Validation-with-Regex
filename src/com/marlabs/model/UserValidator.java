package com.marlabs.model;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class UserValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		User user = (User) target;
		String password = user.getPassword();
		if (!password.matches("[a-z\\d]+"))
			errors.rejectValue("password", null, "password only allow lowercase letters and numerical digits");
		else if (!password.matches(".*\\d.*"))
			errors.rejectValue("password", null, "password must have least one of digitals");
		else if (!password.matches(".*[a-z].*"))
			errors.rejectValue("password", null, "password must have least one of letters");
		else if (!password.matches(".{5,12}"))
			errors.rejectValue("password", null, "password must be between 5 and 12 characters in length");
		else if (password.matches(".*(.+)\\1.*"))
			errors.rejectValue("password", null,
					"password must not contain any sequence of characters immediately followed by the same sequence");
	}

}
